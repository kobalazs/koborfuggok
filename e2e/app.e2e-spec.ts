import { KoborfuggokPage } from './app.po';

describe('koborfuggok App', () => {
  let page: KoborfuggokPage;

  beforeEach(() => {
    page = new KoborfuggokPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
