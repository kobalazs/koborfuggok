import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {Http} from '@angular/http';
import {Ng2Bs3ModalModule} from 'ng2-bs3-modal/ng2-bs3-modal';

import {routing} from './app.routes';
import {
    ApiService,
    CartService,
    CategoryService,
    ConfigService,
    MediaService,
    PageService,
    PostService,
    TagService
} from './services';
import {
    AppComponent,
    EmptyComponent,
    CartComponent,
    CategoryCardComponent,
    CategoryPageComponent,
    NavigationComponent,
    PageComponent,
    ProductItemModalComponent,
    ProductCardComponent,
    ProductPageComponent
} from './components';
import {
    ProductItem
} from './models';


@NgModule({
    declarations: [
        // Components
        AppComponent,
        EmptyComponent,
        CartComponent,
        CategoryCardComponent,
        CategoryPageComponent,
        NavigationComponent,
        PageComponent,
        ProductItemModalComponent,
        ProductCardComponent,
        ProductPageComponent
    ],
    imports: [
        routing,
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule,
        Ng2Bs3ModalModule
    ],
    providers: [
        ApiService,
        CartService,
        CategoryService,
        ConfigService,
        MediaService,
        PageService,
        PostService,
        TagService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }