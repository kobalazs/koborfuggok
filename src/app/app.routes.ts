import {ModuleWithProviders}  from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {
    EmptyComponent,
    CartComponent,
    PageComponent,
    CategoryPageComponent,
    ProductPageComponent,
    ProductItemModalComponent
} from './components';

const routes: Routes = [
    { path: 'kosar', component: CartComponent },
    { path: 'kategoria/:slug', component: CategoryPageComponent },
    { path: 'termek/:slug', component: ProductPageComponent,
        children: [
            { path: '', component: EmptyComponent },
            { path: ':caption-slug', component: ProductItemModalComponent }
        ]
    },
    { path: ':slug', component: PageComponent },
    { path: '', component: PageComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);