import {Component, OnInit} from '@angular/core';

import {CartService, PageService} from '../../services';
import {CartItem} from '../../models';

@Component({
    selector: 'home-component',
    templateUrl: 'cart.html',
    styleUrls: ['cart.css']
})

export class CartComponent {
    public items: CartItem[] = [];
    public name: string;
    public email: string;
    public phone: string;
    public comment: string;
    public delivery;
    public deliveryDetails;
    public deliveryZip;
    public deliveryTown;
    public deliveryAddress;
    
    public page;
    
    public constructor(private _cartService: CartService, private _pageService: PageService) {
        this.items = this._cartService.items;
        
        this._pageService.index({slug: 'szallitas'}).subscribe(
            response => this.page = response.json()[0],
            error => this._pageService.handleError(error)
        );
    }
    
    public removeItem(cartItem: CartItem) {
        this._cartService.removeItem(cartItem.id);
    }
    
    public clearCart() {
        this._cartService.clear();
        this.items = [];
        this.name = undefined;
        this.email = undefined;
        this.phone = undefined;
        this.comment = undefined;
        this.delivery = undefined;
        this.deliveryDetails = undefined;
        this.deliveryZip = undefined;
        this.deliveryTown = undefined;
        this.deliveryAddress = undefined;
    }
    
    public onSubmit() {
        let contact = {
            name: this.name,
            email: this.email,
            phone: this.phone,
            comment: this.comment
        };
        let delivery = {
            delivery: this.delivery,
            deliveryDetails: this.deliveryDetails,
            deliveryZip: this.deliveryZip,
            deliveryTown: this.deliveryTown,
            deliveryAddress: this.deliveryAddress
        };
        this._cartService.send(contact, delivery).subscribe(
            response => {
                if (response.json().success) {
                    window.alert('Köszönjük megrendelésedet! A visszaigazolást elküldtük az általad megadott e-mail címre.');
                    this.clearCart();
                } else {
                    window.alert('A megrendelés elküldése nem sikerült. Kérlek próbáld újra kicsit később!');
                }
            },
            error => {
                window.alert('A megrendelés elküldése nem sikerült. Kérlek próbáld újra kicsit később!');
            }
        );
    }
    
}