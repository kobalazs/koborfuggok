import {Component, OnInit} from '@angular/core';
import {Router}      from '@angular/router';

import {MediaService} from '../../services/media.service';
import {TagService} from '../../services/tag.service';

@Component({
    selector: 'category-card-component',
    templateUrl: 'category-card.html',
    styleUrls: ['category-card.css'],
    inputs: ['page']
})

export class CategoryCardComponent {
    
    public page;
    public tags;
    public featuredMedia;
    
    public constructor(private _router: Router, private _mediaService: MediaService, private _tagService: TagService) {
        
    }
    
    public ngOnInit() {
        this._mediaService.show(this.page.featured_media).subscribe(
            response => this.featuredMedia = response.json(),
            error => this._mediaService.handleError(error)
        );
        this._tagService.index(this.page.id).subscribe(
            response => this.tags = response.json(),
            error => this._tagService.handleError(error)
        );
    }
    
    public gotoCategoryPage() {
        this._router.navigate(['/kategoria', this.page.slug]);
    }
    
}