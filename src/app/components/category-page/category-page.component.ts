import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {CategoryService} from '../../services/category.service';
import {PageService} from '../../services/page.service';
import {PostService} from '../../services/post.service';
import {ProductCardComponent} from '../product-card/product-card.component';

@Component({
    selector: 'home-component',
    templateUrl: 'category-page.html',
    styleUrls: ['category-page.css']
})

export class CategoryPageComponent implements OnInit {
    
    public slug;
    public category;
    public page;
    public posts;
    
    public constructor(private _route: ActivatedRoute, private _categoryService: CategoryService, private _pageService: PageService, private _postService: PostService) {
        
    }
    
    public ngOnInit() {
        this._route.params.subscribe(params => {
            this.slug = params['slug'];
            
            this._categoryService.index({slug: this.slug}).subscribe(
                response => {
                    this.category = response.json()[0];
                    if (this.category) {
                        this._postService.index({categories: this.category.id, order: 'asc', orderby: 'title', per_page: 100}).subscribe(
                            response => this.posts = response.json(),
                            error => this._postService.handleError(error)
                        );
                    } else {
                        this.posts = [];
                    }
                },
                error => this._postService.handleError(error)
            );
            this._pageService.index({slug: this.slug}).subscribe(
                response => this.page = response.json()[0],
                error => this._pageService.handleError(error)
            );
        });
    }
    
}