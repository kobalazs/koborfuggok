import {Component, Input, ChangeDetectionStrategy} from '@angular/core';

import {CartService} from '../../services';

@Component({
    selector: 'navigation-component',
    templateUrl: 'navigation.html',
    changeDetection: ChangeDetectionStrategy.Default
})

export class NavigationComponent {
    
    public constructor(public cartService: CartService) {
        //
    }
}