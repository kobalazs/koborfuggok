import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {PageService} from '../../services/page.service';

@Component({
    selector: 'home-component',
    templateUrl: 'page.html',
    styleUrls: ['page.css']
})

export class PageComponent {
    public slug;
    public page;
    public subPages;
    
    public constructor(private _route: ActivatedRoute, private _pageService: PageService) {
        
    }
    
    public ngOnInit() {
        this._route.params.subscribe(params => {
            this.slug = params['slug'] ? params['slug'] : 'kollekciok';

            this._pageService.index({slug: this.slug}).subscribe(
                response => {
                    this.page = response.json()[0];
                    
                    this._pageService.index({parent: this.page.id, order: 'asc', orderby: 'menu_order'}).subscribe(
                        response => this.subPages = response.json(),
                        error => this._pageService.handleError(error)
                    );
            
                },
                error => this._pageService.handleError(error)
            );
        });
    }
    
}