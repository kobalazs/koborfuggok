import {Component, OnInit} from '@angular/core';
import {Router}      from '@angular/router';

import {MediaService} from '../../services/media.service';
import {TagService} from '../../services/tag.service';

@Component({
    selector: 'product-card-component',
    templateUrl: 'product-card.html',
    styleUrls: ['product-card.css'],
    inputs: ['post']
})

export class ProductCardComponent {
    
    public post;
    public tags;
    public featuredMedia;
    
    public constructor(private _router: Router, private _mediaService: MediaService, private _tagService: TagService) {
        
    }
    
    public ngOnInit() {
        this._mediaService.show(this.post.featured_media).subscribe(
            response => this.featuredMedia = response.json(),
            error => this._mediaService.handleError(error)
        );
        this._tagService.index(this.post.id).subscribe(
            response => this.tags = response.json(),
            error => this._tagService.handleError(error)
        );
    }
    
    public gotoProductPage(slug) {
        this._router.navigate(['/termek', slug]);
    }
    
}