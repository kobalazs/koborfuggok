import {Component, OnInit, ViewChild, HostListener} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalComponent} from 'ng2-bs3-modal/ng2-bs3-modal';

import {CartService, MediaService, PostService} from '../../services';
import {Product, ProductItem, CartItem} from '../../models';

declare var jQuery: any;

@Component({
    selector: 'item-modal-component',
    templateUrl: 'product-item-modal.html',
    styleUrls: ['product-item-modal.css']
})

export class ProductItemModalComponent {
    public product: Product;
    public parentSlug: string;
    public captionSlug: string;
    public items: ProductItem[];
    public previousItem: ProductItem;
    public item: ProductItem;
    public nextItem: ProductItem;
    public isInCart: boolean = false;
    
    @ViewChild('itemModal')
    modal: ModalComponent;
    
    public constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _postService: PostService,
        private _mediaService: MediaService,
        private _cartService: CartService
    ) {
        //
    }
    
    public ngOnInit() {
        this._route.parent.params.subscribe(params => {
            this.parentSlug = params['slug'];
        });
        this._route.params.subscribe(params => {
            this.captionSlug = params['caption-slug'];
            this.loadProduct();
            this.loadItems();
        });
    }
    
    public loadProduct() {
        let lastResponse = this._postService.getLastResponse();
        if (lastResponse) {
            this.parseProduct(lastResponse);
        } else {
            this._postService.lastResponseBehaviour.subscribe(
                response => {
                    if (response != undefined) {
                        this.parseProduct(response);
                    }
                },
                error => {
                    console.log(error);
                }
            );
        }
    }
    
    public parseProduct(lastResponse: any) {
        this.product = new Product().deserialize(lastResponse);
    }
    
    public loadItems() {
        let lastResponse = this._mediaService.getLastResponse();
        if (lastResponse) {
            this.parseItems(lastResponse);
        } else {
            this._mediaService.lastResponseBehaviour.subscribe(
                response => {
                    if (response != undefined) {
                        this.parseItems(response);
                    }
                },
                error => {
                    console.log(error);
                }
            );
        }
    }
    
    public parseItems(lastResponse: any) {
        this.items = ProductItem.hydrate<ProductItem>(ProductItem, lastResponse);
        [this.previousItem, this.item, this.nextItem] = ProductItem.findByCaptionSlug(this.captionSlug, this.items);
        this.item = this.updateFromCart(this.item);
        
        let backdrop = jQuery('.modal-backdrop');
        if (backdrop.length) {
            backdrop.remove();
        }
        this.modal.open();
    }
    
    public gotoItem(item: ProductItem) {
        if (item) {
            this._router.navigate(['/termek', this.parentSlug, item.getCaptionSlug()]);
        }
    }
    
    public updateFromCart(item: ProductItem): ProductItem {
        let cartItem = this._cartService.getItem(item.id);
        if (cartItem) {
            this.isInCart = true;
            return cartItem.productItem;
        }
        this.isInCart = false;
        return item;
    }
    
    public addToCart(item: ProductItem) {
        let cartItem = new CartItem(this.product, item);
        this._cartService.setItem(cartItem);
        this.isInCart = true;
    }
    
    public amountChange(amount: number) {
        if (this.isInCart && amount <= 0) {
            this._cartService.removeItem(this.item.id);
            this.item.amount = 1;
            this.isInCart = false;
        } else {
            this._cartService.storeItems();
        }
    }
    
    @HostListener('window:keydown', ['$event'])
    public eventHandler(event) {
        if (event.target == jQuery('modal')[0]) {
            switch (event.key) {
                case 'ArrowLeft':
                    this.gotoItem(this.previousItem);
                    break;
                case 'ArrowRight':
                    this.gotoItem(this.nextItem);
                    break;
            }
        }
    }
}