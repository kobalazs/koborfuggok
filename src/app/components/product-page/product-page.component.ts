import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {PostService, MediaService, TagService} from '../../services';
import {ProductItem} from '../../models';
import {ProductItemModalComponent} from '../product-item-modal/product-item-modal.component';

@Component({
    selector: 'product-page-component',
    templateUrl: 'product-page.html',
    styleUrls: ['product-page.css']
})

export class ProductPageComponent {
    
    public slug;
    public post;
    public tags;
    public items: ProductItem[];
    public itemsInColumns: Array<Array<ProductItem>>;
    
    public constructor(
        private _route: ActivatedRoute,
        private _postService: PostService,
        private _mediaService: MediaService,
        private _tagService: TagService
    ) {
        //
    }
    
    public ngOnInit() {
        this._route.params.subscribe(params => {
            this.slug = params['slug'];

            this._postService.index({slug: this.slug}).subscribe(
                response => {
                    this.post = response.json()[0];
                    this._postService.setLastResponse(this.post);
            
                    this._mediaService.index(this.post.id).subscribe(
                        response => {
                            let responseJson = response.json();
                            this._mediaService.setLastResponse(responseJson);
                            this.items = ProductItem.hydrate<ProductItem>(ProductItem, responseJson);
                            this.itemsInColumns = this.getItemsInThreeColumns();
                        },
                        error => this._mediaService.handleError(error)
                    );
                    this._tagService.index(this.post.id).subscribe(
                        response => this.tags = response.json(),
                        error => this._tagService.handleError(error)
                    );
                },
                error => this._postService.handleError(error)
            );
        });
    }
    
    public getItemsInThreeColumns() {
        let columns = [[], [], []];
        let count = this.items.length;
        for (let i = 0; i < count; i++) {
            let column = Math.max(0, Math.ceil(3 * (i / count)) - 1);
            columns[column].push(this.items[i]);
        }
        return columns;
    }
    
    public gotoProductItem(productItem: ProductItem) {
        
    }
    
}