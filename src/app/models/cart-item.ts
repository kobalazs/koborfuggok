import {Model, Product, ProductItem} from '../models';

export class CartItem extends Model {
    public id: number;
    public product: Product;
    public productItem: ProductItem;
    
    public constructor(product?: Product, productItem?: ProductItem) {
        super();
        if (product) {
            this.product = product;
        }
        if (productItem) {
            this.productItem = productItem;
            this.id = productItem.id;
        }
    }
    
    public deserialize(data: {[key: string]: any}): any {
        super.deserialize(data);
        //this.product = new Product().deserialize(this.product);
        this.productItem = new ProductItem().deserialize(this.productItem);
        return this;
    }
    
}
