export class Model {
    
    public constructor() {
        //
    }

    public serialize(): {[key: string]: any} {
        let data: any = {};
        for (let key in this) {
            if (typeof this[key] == 'object') {
                data[key] = JSON.parse(JSON.stringify(this[key]));
            } else {
                data[key] = this[key];
            }
        }
        return data;
    }

    public deserialize(data: {[key: string]: any}): any {
        for (let key in data) {
            if (typeof data[key] == 'object') {
                this[key] = JSON.parse(JSON.stringify(data[key]));
            } else {
                this[key] = data[key];
            }
        }
        return this;
    }
    
    public static hydrate<T>(className, modelsData: any[]): T[] {
        let models = [];
        if (modelsData) {
            for (let modelData of modelsData) {
                models.push(new className().deserialize(modelData));
            }
        }
        return models;
    }
    
    public static dehydrate(models: Model[]) {
        let data: any[] = [];
        if (models) {
            for (let model of models) {
                data.push(model.serialize());
            }
        }
        return data;
    }
}