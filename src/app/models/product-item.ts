import {Model} from '../models';

export class ProductItem extends Model {
    public id: number;
    public post: number;
    public caption: {[key: string]: string};
    public description: {[key: string]: string};
    public amount: number = 1;
    
    public constructor() {
        super();
    }
    
    public getCaption() {
        return this.htmlToText(this.caption['rendered']);
    }
    
    public getDescription() {
        return this.htmlToText(this.description['rendered']);
    }
    
    public htmlToText(html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.textContent || div.innerText || '';
    }
    
    public getCaptionSlug() {
        return this.slugify(this.getCaption());
    }
    
    private slugify(text) {
      return text.toString().toLowerCase()
        .replace(/[áąä]/g, 'a')
        .replace(/[éë]/g, 'e')
        .replace(/[íï]/g, 'i')
        .replace(/[óöő]/g, 'o')
        .replace(/[úüű]/g, 'u')
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    }
    
    public static findByCaptionSlug(captionSlug: string, items: ProductItem[]): [ProductItem, ProductItem, ProductItem] {
        let prevActNextItems: [ProductItem, ProductItem, ProductItem] = [undefined, undefined, undefined];
        let i = 0;
        while (i < items.length && !prevActNextItems[2]) {
            if (prevActNextItems[1]) {
                prevActNextItems[2] = items[i];
            } else if (items[i].getCaptionSlug() == captionSlug) {
                prevActNextItems[1] = items[i];
            } else {
                prevActNextItems[0] = items[i];
            }
            i++;
        }
        return prevActNextItems;
    }
}
