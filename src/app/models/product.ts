import {Model} from '../models';

export class Product extends Model {
    public id: number;
    public title: any;
    public content: string;
    public excerpt: string;
    public slug: string;
    public categories: number[];
    public tags: number[];
    
    public constructor() {
        super();
    }
    
    public deserialize(data: {[key: string]: any}): any {
        super.deserialize(data);
        this.title = data['title']['rendered'];
        this.content = data['content']['rendered'];
        this.excerpt = data['excerpt']['rendered'];
        return this;
    }
}
