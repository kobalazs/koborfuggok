import {Injectable} from '@angular/core';
import {Headers, RequestOptions} from '@angular/http';
import {BehaviorSubject} from 'rxjs';

@Injectable()

export class ApiService {
    protected _lastResponse: any;
    public lastResponseBehaviour: BehaviorSubject<any> = new BehaviorSubject(undefined);

    constructor() {
        //
    }

    getJsonHeaders(token?: string) {
        let headers = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
        
        if (token) headers.append('Authorization', 'Bearer ' + token);
        
        return new RequestOptions({headers: headers});
    }
    
    public handleError(error) {
        if (error.json().error) {
            window.alert(error.json().error);
        }
        console.log(error);
    }
    
    public getLastResponse(): any {
        return this._lastResponse;
    }
    
    public setLastResponse(response: any) {
        this._lastResponse = response;
        this.lastResponseBehaviour.next(response);
    }
    
    public resetLastResponse() {
        this._lastResponse = undefined;
    }
}