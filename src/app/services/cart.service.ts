import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import {CartItem, ProductItem} from '../models';
import {ConfigService} from './config.service';

@Injectable()

export class CartService {
    public items: CartItem[] = [];
    
    public constructor(private _config: ConfigService, private _http: Http) {
        this.loadItems();
    }
    
    public loadItems() {
        let storageItem = window.localStorage.getItem('cart');
        let cart = storageItem ? JSON.parse(storageItem) : {items: []};
        if (cart) {
            this.items = CartItem.hydrate<CartItem>(CartItem, cart.items);
        }
    }
    
    public storeItems() {
        window.localStorage.setItem('cart', JSON.stringify({items: this.items}));
    }
    
    public setItem(item: CartItem) {
        for (let i in this.items) {
            if (this.items[i].id == item.id) {
                this.items[i] = item;
                return this;
            }
        }
        // else
        this.items.push(item);
        this.storeItems();
        return this;
    }
    
    public getItem(id: number): CartItem {
        for (let item of this.items) {
            if (item.id == id) {
                return item;
            }
        }
        return undefined;
    }
    
    public removeItem(id: number) {
        for (let i in this.items) {
            if (this.items[i].id == id) {
                this.items.splice(parseInt(i), 1);
                break;
            }
        }
        this.storeItems();
    }
    
    public clear() {
        this.items = [];
        window.localStorage.removeItem('cart');
    }
    
    public send(contact: any, delivery: any) {
        return this._http.post(
            this._config.MAILER_ENDPOINT,
            {
                contact: contact,
                delivery: delivery,
                items: this.items
            },
            []
        );
    }
}