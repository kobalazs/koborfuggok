export class ConfigService {
    API_ENDPOINT: string = 'http://www.koborfuggok.hu/backend/wp-json/wp/v2/';
    MAILER_ENDPOINT: string = 'http://www.example.com/mailer-endpoint';
    // I intentionally changed the mailer endpoint, so no one would accidentally spam the production mailer
}
