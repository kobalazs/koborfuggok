import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import {ApiService} from './api.service';
import {ConfigService} from './config.service';

@Injectable()

export class MediaService extends ApiService {
    
    public constructor(private _config: ConfigService, private _http: Http) {
        super();
    }
    
    public index(parentId?: number) {
        return this._http.get(
            this._config.API_ENDPOINT + 'media/?per_page=100&' + (parentId ? 'parent=' + parentId : ''),
            []
        );
    }
    
    public create() {
        
    }
    
    public store() {
        
    }
    
    public show(id: number) {
        return this._http.get(
            this._config.API_ENDPOINT + 'media/' + id,
            []
        );
    }
    
    public edit() {
        
    }
    
    public update() {
        
    }
    
    public destroy() {
        
    }
}