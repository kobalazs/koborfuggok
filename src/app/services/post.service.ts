import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import {ApiService} from './api.service';
import {ConfigService} from './config.service';

@Injectable()

export class PostService extends ApiService {
    
    public constructor(private _config: ConfigService, private _http: Http) {
        super();
    }
    
    public index(parameters?: Object) {
        let query = '?';
        if (parameters) {
            for (let key in parameters) {
                query = query + encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key]) + '&';
            }
        }
        
        return this._http.get(
            this._config.API_ENDPOINT + 'posts/' + query,
            []
        );
    }
    
    public create() {
        
    }
    
    public store() {
        
    }
    
    public show(id: number) {
        return this._http.get(
            this._config.API_ENDPOINT + 'posts/' + id,
            []
        );
    }
    
    public edit() {
        
    }
    
    public update() {
        
    }
    
    public destroy() {
        
    }
}