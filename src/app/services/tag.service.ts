import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import {ApiService} from './api.service';
import {ConfigService} from './config.service';

@Injectable()

export class TagService {
    
    public constructor(private _api: ApiService, private _config: ConfigService, private _http: Http) {
        
    }
    
    public index(postId?: number) {
        return this._http.get(
            this._config.API_ENDPOINT + 'tags/' + (postId ? '?post=' + postId : ''),
            []
        );
    }
    
    public create() {
        
    }
    
    public store() {
        
    }
    
    public show(id: number) {
        return this._http.get(
            this._config.API_ENDPOINT + 'tags/' + id,
            []
        );
    }
    
    public edit() {
        
    }
    
    public update() {
        
    }
    
    public destroy() {
        
    }

    public handleError(error) {
        this._api.handleError(error);
    }
}